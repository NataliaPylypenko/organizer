<?php
require_once 'organizer.php';

$db = new PDO('mysql:host=localhost;dbname=organizer', 'root', '');
$db->exec("set names utf-8");


$date = new \DateTime();
$last_monday = $date->modify('last Monday');
// $last_monday = $date->format('Y-m-d');

 if (isset($_POST['save-task'])) {
    $date = $_POST['date'];
    $plan = trim($_POST['plan']);
    if (!getPlanByDate($date, $db)) {
        save($plan, $date, $db);
    } else {
        change($plan, $date, $db);
    }
}


$current_week = [
	0 => [date('Y-m-d', strtotime('Mon this week')), 'Понеділок'],
	1 => [date('Y-m-d', strtotime('Tue this week')), 'Вівторок'],
	2 => [date('Y-m-d', strtotime('Wed this week')), 'Середа'],
	3 => [date('Y-m-d', strtotime('Thu this week')), 'Четвер'],
	4 => [date('Y-m-d', strtotime('Fri this week')), 'Пятниця'],
	5 => [date('Y-m-d', strtotime('Sat this week')), 'Субота'],
	6 => [date('Y-m-d', strtotime('Sun this week')), 'Неділля']
];


$plans = getAll($db);



?>