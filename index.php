<?php
require_once 'app.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
<div class="container">
    <h2>Органайзер</h2>

    <ul class="nav nav-pills">
        <?php foreach ($current_week as $day): ?>
            <li <?php if (date('Y-m-d') === $day[0]) {
                echo "class='active'";
            } ?> >
                <a data-toggle="pill" href="#<?= $day[0] ?>"><?= $day[1] ?></a></li>
        <?php endforeach; ?>
    </ul>
    <p align="right"><b>Сегодня:</b> <?php echo date('d.m.Y') ?>;</p>
    <div class="tab-content">
        <?php foreach ($current_week as $day): ?>
            <div id="<?= $day[0] ?>" class="tab-pane fade <?php if (date('Y-m-d') === $day[0]) {
                echo 'in active';
            } ?>">
                <form action="" method="POST">
                    <input type="hidden" name="date" value="<?= $day['0'] ?>">
                    <textarea class="form-control" rows="10" cols="45"
                              name="plan"><?= getPlanByDate($day['0'], $db)['plan'] ?></textarea>
                    <input class="btn-save-task" type="submit" value="Сохранить" name="save-task">
                </form>
            </div>
        <?php endforeach; ?>
    </div>
</div>

</body>
</html> 
