<?php

function getAll($db)
{
    /* Выполнение запроса с передачей ему массива параметров */
    $sql = "SELECT * FROM organizer ORDER BY `date`";
    $sth = $db->prepare($sql);
    $sth->execute();
    $data = $sth->fetchAll(PDO::FETCH_ASSOC);
    return $data;
}


function getPlanByDate($date, $db)
{
    /* Выполнение запроса с передачей ему массива параметров */
    $sql = "SELECT * FROM organizer WHERE `date` = ?";
    $sth = $db->prepare($sql);
    $sth->execute([$date]);
    $data = $sth->fetch(PDO::FETCH_ASSOC);
    return $data;
}

function save($plan, $date, $db)
{
    $stmt = $db->prepare("INSERT INTO organizer (`plan`, `date`) VALUES (:plan, :date)");
    $stmt->bindParam(':plan', $plan, PDO::FETCH_ASSOC);
    $stmt->bindParam(':date', $date, PDO::FETCH_ASSOC);
    $stmt->execute();
    return true;
}

function change($plan, $date, $db)
{
    $stmt = $db->prepare("UPDATE organizer SET `plan`=:plan  WHERE `date` =:date");
    $stmt->bindParam(':plan', $plan);
    $stmt->bindParam(':date', $date);
    $stmt->execute();

}